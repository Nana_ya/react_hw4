import { createContext, useState, useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { ADD_TO_BASKET,
  DELETE_FROM_BASKET,
  ADD_TO_LIKED, 
  DELETE_FROM_LIKED} from "../redux/actions";

export const FunctionContext = createContext(null);

export const FunctionProvider = ({children}) => {
    const basket = useSelector(state => state.basket);
    const likedItems = useSelector(state => state.likedItems);
    const dispatch = useDispatch();
  
    useEffect(() => {
      localStorage.setItem("likedItems", JSON.stringify(likedItems));
    }, [likedItems])
  
    useEffect(() => {
      localStorage.setItem("basket", JSON.stringify(basket));
    }, [basket])
    
    function basketUser(item){
      dispatch({type: ADD_TO_BASKET, payload: item});
    }
  
    function deleteFromBasketUser(item){
      dispatch({type: DELETE_FROM_BASKET, payload: item});
    }
    function likedProduct(item){
      dispatch({type: ADD_TO_LIKED, payload: item});
    }
    function unlikedProduct(item){
      dispatch({type: DELETE_FROM_LIKED, payload: item});
    }

    const value = {basket, likedItems, basketUser, deleteFromBasketUser, likedProduct, unlikedProduct}

    return (
      <FunctionContext.Provider value={value}>
        {children}
      </FunctionContext.Provider>
    )
}