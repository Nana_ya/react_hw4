
import React, { useState, useCallback, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import ButtonModal from './ButtonModal';
import Cards from './Cards';
import CardInfo from './CardInfo';
import ModalWindow from './ModalWindow';
import { Routes, Route, Link } from 'react-router-dom';
import { useFunction } from '../hook/useFunction';


export default function HomePage() {
  const {likedItems} = useFunction();
  // const {basketUser} = useFunction();
  const {unlikedProduct} = useFunction();
  const {likedProduct} = useFunction();

    return (
      <>
        <div>
          <Cards likedItems={likedItems} likedProduct={likedProduct} unlikedProduct={unlikedProduct}/>
        </div>
      </>
    );
}