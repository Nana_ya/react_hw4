import { ADD_TO_BASKET,
DELETE_FROM_BASKET } from "../actions";

export function basketChangerReducer(state = JSON.parse(localStorage.getItem('basket')) || [], action) {
    switch (action.type) {
        case ADD_TO_BASKET:
            let copy = false;
            state.forEach(el => {
                if (el.articul === action.payload.articul){
                copy = true;
                }
            })
            if(!copy){
                return [...state, action.payload];
            }
            else {return state;}
            
        case DELETE_FROM_BASKET:
            state = JSON.parse(localStorage.getItem('basket')).filter(product => product.articul !== action.payload.articul);
            return state;
        default:
            return state;
    }
}