import { GET_PRODUCTS_FROM_JSON } from "../actions";
import { useDispatch } from "react-redux";

export function productsReducer(state = [], action) {
    switch (action.type) {
        case 'GET_PRODUCTS_FROM_JSON':
            state = action.payload
            return state;
        default:
            return state
    }
}