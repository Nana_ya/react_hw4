import React, { useState, useEffect } from 'react';
import PropTypes from "prop-types";
import { Button } from 'react-bootstrap';
import CardInfo from '../components/CardInfo';
import ModalWindow from '../components/ModalWindow';
import { useFunction } from '../hook/useFunction';
import { useSelector, useDispatch } from 'react-redux';
import { MODAL_WINDOW_SHOW_TRUE, 
    MODAL_WINDOW_SHOW_FALSE,
    ADD_TO_BASKET, 
    DELETE_FROM_BASKET } from "../redux/actions";

export default function CartPage() {
    const {deleteFromBasketUser} = useFunction();
    const [lastChosenProduct, setLastChosenProduct] = useState({});
    const show = useSelector(state => state.show);
    const basket = useSelector(state => state.basket);
    const dispatch = useDispatch();

    const textFirstModal = "Are you sure you want to delete this product from the cart?";
    const headerFirstModal = "Are you sure?";
    return (
        <div className='cards'>
            {basket?.map((product) => (
                
                <div className="card" key={product.articul}>
                    
                    {<ModalWindow
                        header={headerFirstModal}
                        text={textFirstModal}
                        lastChosenProduct={lastChosenProduct}
                        closeButton={true}
                        color="red"
                        actions={
                            <>
                                <Button className='modal-button-left' onClick={ () => {
                                    
                                    deleteFromBasketUser(lastChosenProduct);
                                    dispatch({type: MODAL_WINDOW_SHOW_FALSE});
                                }}>Ok
                                </Button>
                                 <Button className='modal-button-right' onClick={() => {dispatch({type: MODAL_WINDOW_SHOW_FALSE});}}>
                                    No
                                </Button>
                            </>
                        }
                        handleShow={() => {dispatch({type: MODAL_WINDOW_SHOW_TRUE});}}
                        handleClose={() => {dispatch({type: MODAL_WINDOW_SHOW_FALSE});}}
                        show1={show}
                        
                        />}
                        <CardInfo 
                            product={product}
                            button={
                                <>
                                <input type="number" id="quantity" name="quantity" min="1" max="30" defaultValue={product.amount}
                                onChange={(e) => {
                                    basket.forEach(el => {
                                        if(el.articul === product.articul) {
                                            el.amount = e.currentTarget.value;
                                        }
                                    })
                                }}/>
                                <svg className="cross-svg"
                                onClick={()=>{
                                    dispatch({type: MODAL_WINDOW_SHOW_TRUE});
                                    setLastChosenProduct(product);
                                }}
                                width="50px" height="50px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M16 8L8 16M8.00001 8L16 16" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
                                </svg>
                                </>
                            } 
                        />
                </div>
            ))}
            
        </div>
    );
}